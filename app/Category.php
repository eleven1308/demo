<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = ['name', 'status', 'order' ,'parent_id' ,'slug','description'];
    public $timestamps = false;

    public function news() {
    	return $this->hasMany('App\News');
    }

 }
