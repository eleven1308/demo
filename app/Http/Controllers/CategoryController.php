<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Categoryrequest;
use App\Category;
use Illuminate\Http\Session;
use App\User;

class CategoryController extends Controller
{
  public function getShow() {
    $Category = Category::orderBy('id','DESC')->get();
    return view('admin.category.show',['Category' => $Category]);

  }

  public function getAdd() {
   $parent = Category::all();
   return view('admin.category.add',['parent'=>$parent]);


 }

 public function postAdd(Categoryrequest $request) {
   $Category = new Category;
   $Category->name =   $request->Catename;
   $Category->slug =   changeTitle($request->Catename); 
   $Category->order =  $request->Cateorder; 
   $Category->parent_id = $request->Cateparent;
   $Category->description =  $request->Catedescription;
   $Category->save();
   session()->flash('message', 'Thêm Thành Công');
   return redirect()->route('admin.category.show');


 }

 public function getDelete($id) {
  $parent = Category::where('parent_id',$id)->count();
  if($parent == 0){
    $category = Category::find($id);
    $category->delete($id);
    session()->flash('message', 'Xóa Thành Công');
    return redirect()->route('admin.category.show');
  }
  else{
    echo "<script type = 'text/javascript'>

         alert('Đây là thư mục gốc, Bạn phải xóa thư mục con chứa nó trước');
         window.location = '";
          echo route('admin.category.show');

         echo"'
      
         </script>"; 
  }


    }

}