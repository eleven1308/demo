<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\News;
use App\Images;
use App\Http\Requests\NewsRequest;

class NewsController extends Controller
{
   public function getShow() {
      $news = News::select('id','name' ,'title' ,'slug','status' ,'desciption', 'images', 'created_at')->orderBy('id','DESC')->get();
       return view('admin.news.show', ['news' =>$news]);
   }
    
  
  
    public function getAdd() {
    	$category = Category::all();
       return view('admin.news.add',['category'=>$category]);

   }
   public function getTest() {
    return view('admin.news.test');
   }

   public function postAdd(NewsRequest $request) {
       $news = new News();
       $news->name = $request->newsname;
       $news->slug = changeTitle($request->newsname);
       $news->title = $request->newstitle;
       $news->desciption = $request->description;
       $news->status = $request->newsstatus;
       $news->user_id = 1;
       $news->category_id = $request->newscategory;

       if ($request->hasFile('images')) {
        $file = $request->file('images');
        $duoi = $file->getClientOriginalExtension();
        if ($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg') {
          session()->flash('message', 'Bạn chỉ có thể chọn hình với đuôi jpg, png, jpeg');
           // return redirect()->route('admin.news.add');
       }
       $name = $file->getClientOriginalName();
       $images = str_random(4)."_".$name;
       while (file_exists("images/news/".$images)) {
         $images = str_random(4)."_".$name;
     }
     $file->move("images/news",$images);
     $news->images = $images;

 }

     $news->save();
     session()->flash('message', 'Thêm Thành Công');
     return redirect()->route('admin.news.show');

}
    public function getDelete($id) {
      $news = News::find($id);
      $news->delete();
      session()->flash('message', 'Bạn đã xóa thành công');
      return redirect()->route('admin.news.show');

    }



}
