<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Categoryrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Catename' => 'required|min:3|unique:category,name',
            'Cateslug' => 'required|unique:category,slug',
            'Cateorder' => 'required|unique:category,order',
            'Catedescription' => 'required|min:3|unique:category,description',
            
        ];
    }

    public function messages() {
          return [
           'Catename.required' => 'Bạn chưa nhập thể loại',
           'Cateslug.required' => 'Slug chưa tồn tại',
           'Cateorder.required' => 'Bạn chưa nhập thể loại',
           'Catedescription.required' => 'Bạn chưa nhập description',
            
        ];

    }
}
