<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
      public function rules()
    {
        return [
            'txtEmail' => 'required|unique:users,email',
            'txtPassword' => 'required|unique:users,password'
            
        ];
    }

    public function messages() {
          return [
           'txtEmail.required' => 'Vui lòng nhập email',
           'txtPassword.required' => 'Vui lòng nhập password',
        ];

    }
}
