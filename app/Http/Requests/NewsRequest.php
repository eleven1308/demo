<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'newsname' => 'required|min:3|unique:news,name',
            'newstitle' => 'required|unique:news,title',
            'description' => 'required|min:3|unique:news,desciption',
            'images' => 'required',
            'newsstatus' => 'required'
            
        ];
    }

    public function messages() {
          return [
           'newsname.required' => 'Bạn chưa nhập thể loại',
           'newstitle.required' => 'Bạn chưa nhập tiêu đề',
           'description.required' => 'Bạn chưa nhập description',
           'images.required' => 'Vui lòng chọn images',
           'newsstatus.required' => 'Trạng thái status không được để trống'
        ];

    }
}
