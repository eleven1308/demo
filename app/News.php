<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = ['name', 'slug', 'title', 'description' ,'images' ,'status','category_id', 'like', 'is_hot' , 'user_id' , 'category_id' ,'views' ];

    public $timestamps = true;

    public function category() {
    	return $this->belongTo('App\Category');
    }
     public function users() {
    	return $this->belongTo('App\Users');
    }
    
     public function images() {
    	return $this->hasMany('App\Images');
    }
}	
