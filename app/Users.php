<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $fillable = ['name', 'avatar', 'email', 'password' ,'phone' ,'address','level', 'status'];
    public $timestamps = false;
    protected $hidden = ['password' , 'remember_token'];

     public function news() {
    	return $this->hasMany('App\News');
    }
}
