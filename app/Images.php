<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'images';
    protected $fillable = ['name', 'image', 'slug', 'description' ,'news_id'];
    public $timestamps = false;

     public function news() {
    	return $this->belongTo('App\News');
    }
}
