<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
     protected $table = 'slide';
    protected $fillable = ['name', 'alias', 'status', 'order' ,'parent_id' ,'slug','description'];

    public $timestamps = false;
    
}
