<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
         public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('title');
            $table->text('desciption');
            $table->string('images');
            $table->float('util_price');
            $table->float('promotion_price');
            $table->integer('status');
            $table->integer('like')->nullable();
            $table->integer('is_hot');
            $table->integer('all_views');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('views')->nullable();
            $table->integer('materials')->nullable();
            $table->timestamps();

        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
