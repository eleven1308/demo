@extends('admin.layout.master')
@section('content')
<aside class="right-side">
 <section class="content">
    <!--main content-->
    <div class="row">
        <!--row starts-->

        <!--md-6 ends-->
        <div class="col-md-8">
            <!--md-6 starts-->
            <!--form control starts-->
            <div class="panel panel-primary" id="hidepanel6">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="share" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                       NEWS                    </h3>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-chevron-up clickable"></i>
                        <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                    </span>
                </div>
                <div class="panel-body">


                 @if(count($errors) > 0)
                 <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                    {{$err}}<br>
                    @endforeach
                </div>
                @endif

              <!--   @if(session('thongbao'))
                <div class="alert alert-success">
                  {{session('thongbao')}}
              </div>
              @endif -->

              @if (session()->has('message'))
              <div class="alert alert-success">
                  <strong>Notification:</strong> {{ session()->get('message') }}            
              </div> 

              @endif

              <form action="" method="POST"  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" name="newsname" placeholder="Enter text" >
                </div>

                <div class="form-group">
                    <label>Slug</label>
                    <input class="form-control" placeholder="Enter text" name ="newsname"></div>
                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" placeholder="Enter text" name ="newstitle">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input class="form-control" placeholder="Enter text" 
                         name ="description" >
                    </div>
                    <div class="form-group">
                        <label>Images</label>
                        <input class="form-control" placeholder="Enter text" name ="images" type="file">
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                        <label class="radio-inline">
                            &nbsp;<input type="radio" class="custom-radio" name="newsstatus" id="newsstatus" value="1" >&nbsp; Hiện</label>
                            <label class="radio-inline">
                                <input type="radio" class="custom-radio" name="newsstatus" id="newsstatus" value="2">&nbsp;Ẩn</label>
                            </div>


                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" name="newscategory">
                                    <option value="0">Vui lòng chọn</option>
                                       <?php category_parent($category); ?>

                                </select>
                            </div>
                            <div class="form-group">
                               <label>Text area</label>
                               <textarea class="form-control ckeditor" rows="3" id="">

                               </textarea>        
                           </div>

                           <div class="col-md-12 mar-10">
                            <div class="col-xs-6 col-md-6">
                                <input type="submit" name="submit" id="btnSubmit" value="Submit" class="btn btn-primary btn-block btn-md btn-responsive">
                            </div>
                            <div class="col-xs-6 col-md-6">
                               <a href=""> <input type="" value="Cancel" class="btn btn-success btn-block btn-md btn-responsive"></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--md-6 ends-->
    </div>

</section>
</aside>
@endsection