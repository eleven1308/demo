@extends('admin.layout.master')
@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--section starts-->
        <h1>Advanced Data Tables</h1>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">
                    <i class="livicon" data-name="home" data-size="14" data-loop="true"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Data Tables</a>
            </li>
            <li class="active">Advanced Data Tables</li>
        </ol>
    </section>
    <!--section ends-->
    <section class="content">

        <!-- row-->
        <div class="row">
            <div class="col-lg-12">
               @if (session()->has('message'))
               <div class="alert alert-success">
                  <strong>Notification:</strong> {{ session()->get('message') }}            
              </div> 

              @endif
              <div class="panel panel-info filterable" style="overflow:auto;">
               <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left add_remove_title">
                    <i class="livicon" data-name="gift" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Table Category
                </h3>
                <div class="pull-right">
                    <a href="{{ route('admin.category.getAdd')}}"> <button type="button" class="btn btn-primary btn-sm" id="addButton">ADD DATA</button></a>

                </div>
            </div>
            <div class="panel-body table-responsive">
                <table class="table table-striped table-bordered" id="table2">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Order(Vị trí)</th>
                            <th>Parent</th>
                            <th>Description</th>
                            <th>Thao Tác</th>
                            <th>Thao Tác</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($Category as $item)
                        <tr>
                            <td>{{ $item->id}}</td>
                            <td>{{ $item->name}}</td>
                            <td>{{ $item->slug}}</td>
                            <td>{{ $item->order}}</td>
                            <td>
                                @if($item['parent_id'] == 0)
                                {{"Thư Mục Cha"}}
                                @else
                                <?php 
                                $parent = DB::table('category')->where('id',$item['parent_id'])->first();
                                echo $parent->name;
                                ?>
                                @endif
                            </td>
                            <td>{{ $item->description}}</td>
                            <td><button type="button" class="btn btn-danger btn-sm" id="de"><a href="delete/{{$item->id}}">DELETE</a></button></td>
                            <td><button type="button" class="btn btn-warning btn-sm" id=""><a href="edit/{{$item->id}}" onclick="return confirm('Sorry, chức năng này chưa update?')" >EDIT</a></button></td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- row-->

<!-- Third Basic Table Ends Here-->
<!--delete modal starts here-->

<!-- /.modal ends here -->
</section>
<!-- content -->
</aside>

@endsection