@extends('admin.layout.master')
@section('content')
<aside class="right-side">
   <section class="content">
    <!--main content-->
    <div class="row">
        <!--row starts-->
        
        <!--md-6 ends-->
        <div class="col-md-8">
            <!--md-6 starts-->
            <!--form control starts-->
            <div class="panel panel-success" id="hidepanel6">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="share" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        CATEGORY 
                    </h3>
                    <span class="pull-right">
                        <i class="glyphicon glyphicon-chevron-up clickable"></i>
                        <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                    </span>
                </div>
                <div class="panel-body">

                   
                   @if(count($errors) > 0)
                   <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                    {{$err}}<br>
                    @endforeach
                </div>
                @endif

                @if(session('thongbao'))
                <div class="alert alert-success">
                  {{session('thongbao')}}
              </div>
              @endif

              <form action="" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" name="Catename" placeholder="Enter text" >
                </div>

                <div class="form-group">
                    <label>Slug</label>
                    <input class="form-control" placeholder="Enter text" name ="Cateslug"></div>
                    <div class="form-group">
                        <label>Order</label>
                        <input class="form-control" placeholder="Enter text" name ="Cateorder"></div>
                        <div class="form-group">
                            <label>Description</label>
                            <input class="form-control" placeholder="Enter text" name ="Catedescription"></div>
                            <div class="form-group">
                                <label>Parent</label>
                                <select class="form-control" name="Cateparent">
                                    <option value="0">Vui lòng chọn</option>
                                    <?php category_parent($parent); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Text area</label>
                                <textarea class="form-control resize_vertical" rows="3"></textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-responsive btn-default">Submit</button>
                            <button type="reset" class="btn btn-responsive btn-default" id="reset"><a href="{{route('admin.news.show')}}">Cancel</a></button>
                        </form>
                    </div>
                </div>
            </div>
            <!--md-6 ends-->
        </div>
        
    </section>
</aside>
@endsection