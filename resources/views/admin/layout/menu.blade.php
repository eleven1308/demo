 <aside class="left-side sidebar-offcanvas">
    <section class="sidebar ">
        <div class="page-sidebar  sidebar-nav">
            <div class="nav_icons">
                <ul class="sidebar_threeicons">
                    <li>
                        <a href="advanced_tables.html">
                            <i class="livicon" data-name="table" title="Advanced tables" data-c="#418BCA" data-hc="#418BCA" data-size="25" data-loop="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="tasks.html">
                            <i class="livicon" data-c="#EF6F6C" title="Tasks" data-hc="#EF6F6C" data-name="list-ul" data-size="25" data-loop="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="gallery.html">
                            <i class="livicon" data-name="image" title="Gallery" data-c="#F89A14" data-hc="#F89A14" data-size="25" data-loop="true"></i>
                        </a>
                    </li>
                    <li>
                        <a href="users_list.html">
                            <i class="livicon" data-name="users" title="Users List" data-size="25" data-c="#01bc8c" data-hc="#01bc8c" data-loop="true"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>
            <!-- BEGIN SIDEBAR MENU -->
            <ul id="menu" class="page-sidebar-menu">
                <li class="active">
                    <a href="">
                        <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i class="livicon" data-name="medal" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Quản Lý Thể Loại</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('admin.category.show')}}">
                                <i class="fa fa-angle-double-right"></i> Danh Sách Thể Loại
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.category.getAdd')}}">
                                <i class="fa fa-angle-double-right"></i> Thêm thể loại
                            </a>
                        </li>
                          <li>
                            <a href=""  >
                                <i class="fa fa-angle-double-right"></i> Sửa thể loại
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="">
                        <i class="livicon" data-name="doc-portrait" data-c="#5bc0de" data-hc="#5bc0de" data-size="18" data-loop="true"></i>
                        <span class="title">Quản Lý Tin Tức</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('admin.news.show')}}">
                                <i class="fa fa-angle-double-right"></i> Danh Sách Tin Tức
                            </a>
                        </li>
                         <li>
                            <a href="{{ route('admin.news.getAdd')}}">
                                <i class="fa fa-angle-double-right"></i> Thêm Tin Tức
                            </a>
                        </li>
                         <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Sửa Tin Tức
                            </a>
                        </li>
                    </ul>
                </li>


              


                <li>
                    <a href="#">
                        <i class="livicon" data-name="lab" data-c="#EF6F6C" data-hc="#EF6F6C" data-size="18" data-loop="true"></i>
                        <span class="title">Quản Lý Slides</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Danh Sách Slide
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Thêm SLide
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Sửa Slide
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="livicon" data-name="table" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                        <span class="title">Quản Lý Người Dùng</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Danh Sách Người Dùng
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Thêm Người Dùng
                            </a>
                        </li>
                        
                    </ul>
                </li>

             


                <li>
                    <a href="#">
                        <i class="livicon" data-name="table" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                        <span class="title">Cấu Hình Website</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="">
                                <i class="fa fa-angle-double-right"></i> Website
                            </a>
                        </li>
                    </ul>
                </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</section>
<!-- /.sidebar -->
</aside>