<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ADMIN</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
   <!--  <base href="{{asset('')}}"> -->
    <link href="{{ asset('public/admin_assets/assets/css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/admin_assets/assets/css/toastr.css') }}" rel="stylesheet" type="text/css">
     <link href="{{ asset('public/admin_assets/assets/css/toastr.min.css') }}" rel="stylesheet" type="text/css">
    <!-- end of global css -->
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/buttons.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/colReorder.bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/dataTables.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/rowReorder.bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/buttons.bootstrap.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('public/admin_assets/assets/vendors/datatables/css/scroller.bootstrap.css')}}" />
    <link href="{{ asset('public/admin_assets/assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css">
   
    <!--end of page level css-->
</head>

<body class="skin-josh">
        @include('admin.includes.header')
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        @include('admin.layout.menu')
        <!-- Right side column. Contains the navbar and content of the page -->
        @yield('content')
        <!-- right-side -->
    </div>
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!-- global js -->
    <script src="{{ asset('public/admin_assets/assets/js/app.js')}}" type="text/javascript"></script>
    <!-- end of global js -->
    <!-- begining of page level js -->
    <!-- EASY PIE CHART JS -->

    <!-- end of page level js -->

    <script src="{{ asset('public/admin_assets/assets/js/app.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/admin_assets/assets/js/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/admin_assets/assets/js/toastr.js.map')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/jeditable/js/jquery.jeditable.js') }}"></script>
    <script type="text/javascript" src="{ asset('public/admin_assets/assets/vendors/datatables/js/dataTables.bootstrap.js') }"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/buttons.print.js')}}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/buttons.bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/vfs_fonts.js')}}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/js/pages/table-advanced.js')}}"></script>
    <script type="text/javascript" src="{{ asset('public/admin_assets/assets/js/myscript.js')}}"></script>
    <script type="text/javascript" language="javascript" src="{{asset('public/admin_assets/assets/ckeditor/ckeditor.js')}}"></script>
    <script src="//cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

</body>

</html>
