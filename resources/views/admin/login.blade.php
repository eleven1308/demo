<!DOCTYPE html>
<html>

<head>
    <title>Admin Login | Josh Admin Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- global level css -->
    <link href="{{ asset('public/admin_assets/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!-- end of global level css -->
    <!-- page level css -->
    <link href="{{ asset('public/admin_assets/assets/css/pages/login2.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/admin_assets/assets/vendors/iCheck/css/minimal/blue.css')}}" rel="stylesheet" />
    <!-- styles of the page ends-->
</head>

<body>
    <div class="container">
        <div class="row vertical-offset-100">
            <div class=" col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3  col-md-5 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Sign In</h3>
                    </div>
                    @if (session()->has('message'))
                    <div class="alert alert-success">
                      <strong>Notification:</strong> {{ session()->get('message') }}            
                  </div> 

                  @endif
                  <div class="panel-body">
                    <form action="" method="POST"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <fieldset>
                            <div class="form-group input-group">
                                <div class="input-group-addon">
                                    <i class="livicon" data-name="mail" data-size="18" data-c="#484848" data-hc="#484848" data-loop="true"></i>
                                </div>
                                <input class="form-control" placeholder="E-mail" name="txtEmail" type="text" required>
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-addon">
                                    <i class="livicon" data-name="key" data-size="18" data-c="#484848" data-hc="#484848" data-loop="true"></i>
                                </div>
                                <input class="form-control" placeholder="Password" name="txtPassword" type="password" value="" required>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me" class="minimal-blue" required> Remember Me
                                </label>
                            </div>
                            <a href="" class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Sign In</a>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="{{ asset('public/admin_assets/assets/js/app.js') }}" type="text/javascript"></script>
<!-- end of global js -->
<!-- begining of page level js-->
<script src="{{ asset('public/admin_assets/assets/js/TweenLite.min.js') }}"></script>
<script src="{{ asset('public/admin_assets/assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script src="{{ asset('public/admin_assets/assets/js/pages/login2.js')}}" type="text/javascript"></script>
<!-- end of page level js-->
</body>

</html>
