<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
Route::get('',function(){
	return view('admin.index');
});

// Route::post('admin','LoginController@getDelete')->name('postLogin');

Route::group(['prefix'=>'admin'],function() {
   
	Route::group(['prefix'=>'category'],function() {
		Route::get('list','CategoryController@getShow')->name('admin.category.show');
		Route::get('add','CategoryController@getAdd')->name('admin.category.getAdd');
		Route::post('add','CategoryController@postAdd')->name('admin.category.posttAdd');
		Route::get('edit','CategoryController@getEdit')->name('admin.category.getEdit');
		Route::post('edit','CategoryController@postEdit')->name('admin.category.postEdit');
		Route::get('delete/{id}','CategoryController@getDelete')->name('admin.category.getDelete');
	});

	Route::group(['prefix'=>'news'],function() {
		Route::get('list','NewsController@getShow')->name('admin.news.show');
		Route::get('add','NewsController@getAdd')->name('admin.news.getAdd');
		Route::post('add','NewsController@postAdd')->name('admin.news.postAdd');
		Route::get('edit','NewsController@getEdit')->name('admin.news.getEdit');
		Route::post('edit','NewsController@postEdit')->name('admin.news.postEdit');
		Route::get('delete/{id}','NewsController@getDelete')->name('admin.news.getDelete');
	});

});
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
